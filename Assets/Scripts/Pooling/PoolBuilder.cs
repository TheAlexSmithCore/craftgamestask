﻿using System.Collections.Generic;
using UnityEngine;

namespace Pooling
{
    public class PoolBuilder<TPoolType> where TPoolType : Object 
    {
        public PoolBuilder(TPoolType prefab, Transform parent, int amount)
        {
            _prefab = prefab;
            _parent = parent;
            _amount = amount;
        }

        private readonly TPoolType _prefab;
        private readonly Transform _parent;
        private readonly int _amount;

        public LinkedList<TPoolType> Build()
        {
            LinkedList<TPoolType> objectsLinkedList = new LinkedList<TPoolType>();
            
            for (int i = 0; i < _amount; i++)
            {
                TPoolType obj = Object.Instantiate(_prefab, Vector3.zero, Quaternion.identity, _parent);
                objectsLinkedList.AddLast(obj);
            }

            return objectsLinkedList;
        }
    }
}