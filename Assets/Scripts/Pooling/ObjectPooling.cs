﻿using System.Collections.Generic;
using UnityEngine;

namespace Pooling
{
    public class ObjectPooling
    {
        public ObjectPooling(GameObject prefab, Transform parent, int poolAmount)
        {
            var poolBuilder = new PoolBuilder<GameObject>(prefab, parent, poolAmount);
            _objects = poolBuilder.Build();

            DisableAll();
        }

        private readonly LinkedList<GameObject> _objects;

        public LinkedList<GameObject> GetList() => _objects;

        public GameObject GetPooledObject()
        {
            foreach (var pooledObject in _objects)
            {
                if (!pooledObject.activeInHierarchy)
                {
                    return pooledObject;
                }    
            }

            return null;
        }

        public GameObject GetFirstPooledObject()
        {
            GameObject firstPooledObject = _objects.First.Value;
            MoveToEnd(firstPooledObject);
            return firstPooledObject;
        }
        
        private void MoveToEnd(GameObject firstObject)
        {
            _objects.RemoveFirst();
            _objects.AddLast(firstObject);
        }

        public void DisableAll()
        {
            foreach (var pooledObject in _objects)
            {
                pooledObject.SetActive(false);
            }
        }
    }
}