﻿using Matchmaking;
using UnityEngine;

namespace CustomPhysics
{
    public class NormalDetector : IMatchStateObserver
    {
        public NormalDetector(RayCaster rayCaster)
        {
            _rayCaster = rayCaster;

            MatchHandler.Register(this);
        }

        ~NormalDetector()
        {
            MatchHandler.Unregister(this);
        }

        private bool _isLocked;
        
        public bool IsDetected { get; private set; }
        
        private Vector3 _normal;
        private readonly RayCaster _rayCaster;

        public Vector3 Get() => _normal;

        public void Detect()
        {
            if(_isLocked) return;

            IsDetected = _rayCaster.Cast();

            if (!IsDetected) return;
            
            RaycastHit hit = _rayCaster.Get();
            _normal = hit.normal;
        }

        public void OnMatchStateChanged(MatchState state)
        {
            switch (state)
            {
                case MatchState.Ended:
                    _isLocked = true;
                    break;
                case MatchState.Started:
                    _isLocked = false;
                    break;
            }
        }
    }
}