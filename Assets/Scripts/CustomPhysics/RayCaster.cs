﻿using UnityEngine;

namespace CustomPhysics
{
    public class RayCaster
    {
        public RayCaster(Transform owner, Vector3 castDirection, LayerMask layerMask, int maxResultsCount = 1)
        {
            _owner = owner;
            _castDirection = castDirection;
            _layerMask = layerMask;
            
            _hitResults = new RaycastHit[maxResultsCount];
        }
        
        private readonly Transform _owner;
        private readonly Vector3 _castDirection;
        private readonly LayerMask _layerMask;
        private readonly RaycastHit[] _hitResults;
        
        public bool Cast()
        {
            Ray ray = new Ray(_owner.position, _castDirection);
            int collisionsCount = Physics.RaycastNonAlloc(ray, _hitResults, _layerMask);
            return collisionsCount > 0;
        }

        public RaycastHit Get() => _hitResults[0];
    }
}