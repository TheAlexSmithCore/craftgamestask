﻿using CustomPhysics;
using Matchmaking;
using UnityEngine;

namespace Movement
{
    public class BallMovement : IMovable, IMatchStateObserver
    {
        public BallMovement(Transform movableObject, NormalDetector normalDetector, MovementParameters movementParameters)
        {
            _movableObject = movableObject;
            _movementParameters = movementParameters;
            
            _normalDetector = normalDetector;
            _surfaceSlider = new SurfaceSlider();
            
            MatchHandler.Register(this);
        }

        private bool _lockGravity;
        
        private Vector3 _movementDirection;

        private readonly Transform _movableObject;
        private readonly MovementParameters _movementParameters;

        private readonly NormalDetector _normalDetector;
        private readonly SurfaceSlider _surfaceSlider;
        
        public void SetDirection(Vector3 direction)
        {
            _movementDirection = direction;
        }
        
        public void Move()
        {
            Vector3 resultDirection = CalculateResultDirection();

            _movableObject.Translate(resultDirection * Time.deltaTime);
        }

        public void TranslateTo(Vector3 point)
        {
            _movableObject.position = point;
        }

        private Vector3 CalculateResultDirection()
        {
            float movementSpeed = _movementParameters.movementSpeed;
            
            if (!_normalDetector.IsDetected)
            {
                Vector3 gravityVector = CalculateGravity();
                return _movementDirection + gravityVector;
            }
            else
            {
                Vector3 normal = _normalDetector.Get();
                return _surfaceSlider.Project(_movementDirection,normal) * movementSpeed;
            }
        }

        private Vector3 CalculateGravity()
        {
            if (_lockGravity)
                return Vector3.zero;
            
            return Vector3.up * Physics.gravity.y;
        }

        public void OnMatchStateChanged(MatchState state)
        {
            switch (state)
            {
                case MatchState.Started:
                    _lockGravity = true;
                    break;
                case MatchState.WaitingForStart:
                    _lockGravity = true;
                    SetDirection(Vector3.zero);
                    break;
                case MatchState.Ended:
                    _lockGravity = false;
                    break;
            }
        }
    }
}