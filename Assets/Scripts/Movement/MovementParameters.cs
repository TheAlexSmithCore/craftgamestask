﻿namespace Movement
{
    [System.Serializable]
    public struct MovementParameters
    {
        public MovementParameters(float movementSpeed)
        {
            this.movementSpeed = movementSpeed;
        }
        
        public float movementSpeed;
    }
}