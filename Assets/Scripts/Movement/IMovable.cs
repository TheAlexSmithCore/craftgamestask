using UnityEngine;

public interface IMovable
{
    public void Move();
    public void TranslateTo(Vector3 point);
}
