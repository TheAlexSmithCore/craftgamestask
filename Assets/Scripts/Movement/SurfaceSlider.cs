﻿using UnityEngine;

namespace Movement
{
    public class SurfaceSlider
    {
        public Vector3 Project(Vector3 direction, Vector3 normal)
        {
            return direction - Vector3.Dot(direction, normal) * normal;
        }
    }
}