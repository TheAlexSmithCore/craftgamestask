﻿using System;
using System.Collections;
using UnityEngine;

namespace Helpers
{
    public class UnityCoroutineHandler : MonoBehaviour
    {
        private static UnityCoroutineHandler _instance;

        private void Awake()
        {
            _instance = this;
        }

        public static Coroutine StartCoroutineRemotely (IEnumerator coroutine)
        {
            return _instance.StartCoroutine(coroutine);
        }

        public static void StopCoroutineRemotely(Coroutine coroutine)
        {
            if (coroutine == null)
            {
                Debug.LogException(new Exception("Cannot stop coroutine remotely. Reason: coroutine is null"));
                return;
            }

            _instance.StopCoroutine(coroutine);
        }
    }
}