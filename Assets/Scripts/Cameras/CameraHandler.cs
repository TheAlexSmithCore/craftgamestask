namespace Cameras
{
    public class CameraHandler
    {
        private ICameraController _currentController;

        public void SetController(ICameraController controller)
        {
            _currentController = controller;
        }

        public void Execute()
        {
            _currentController?.Move();
        }
    }
}
