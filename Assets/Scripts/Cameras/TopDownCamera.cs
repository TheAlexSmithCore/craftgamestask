using Matchmaking;
using UnityEngine;

namespace Cameras
{
    public class TopDownCamera : ICameraController, IMatchStateObserver
    {
        public TopDownCamera(Transform cameraTransform, Transform target, CameraParameters parameters)
        {
            _cameraTransform = cameraTransform;
            _target = target;
            _parameters = parameters;

            UpdateCameraAngle();
            
            MatchHandler.Register(this);
        }

        private bool _lockCamera;
        private Quaternion _cameraAngleAxis;

        private readonly Transform _cameraTransform;
        private readonly Transform _target;
        private readonly CameraParameters _parameters;

        private void UpdateCameraAngle()
        {
            _cameraAngleAxis = Quaternion.AngleAxis(_parameters.angle, Vector3.up);
        }

        public void Move()
        {
            if (_lockCamera) return;

            Vector3 targetPosition = _target.position;
            Vector3 flatTargetPosition = GetFlatPosition(targetPosition);
            Vector3 finalPosition = flatTargetPosition + HandleCameraToTarget();
            
            _cameraTransform.transform.position = finalPosition;
            _cameraTransform.transform.LookAt(flatTargetPosition);
        }

        private Vector3 GetFlatPosition(Vector3 position)
        {
            Vector3 newPosition = position;
            newPosition.y = 0;
            return newPosition;
        }

        private Vector3 HandleCameraToTarget()
        {
            UpdateCameraAngle();
            
            Vector3 forwardDirection = Vector3.forward * -_parameters.distance;
            Vector3 upDirection = Vector3.up * _parameters.height;
            Vector3 worldPosition = forwardDirection + upDirection;
            Vector3 worldRotation = _cameraAngleAxis * worldPosition;
            
            return worldRotation;
        }

        public void OnMatchStateChanged(MatchState state)
        {
            switch (state)
            {
                case MatchState.WaitingForStart:
                    _lockCamera = false;
                    break;
                case MatchState.Ended:
                    _lockCamera = true;
                    break;
            }
        }
    }
}
