using UnityEngine;

namespace Cameras
{
    [System.Serializable]
    public struct CameraParameters
    {
        public CameraParameters(float height, float distance, float angle, Vector3 offset)
        {
            this.height = height;
            this.distance = distance;
            this.angle = angle;
            this.offset = offset;
        }
        
        public CameraParameters(float height, float distance, float angle)
        {
            this.height = height;
            this.distance = distance;
            this.angle = angle;
            offset = Vector3.zero;
        }
        
        public float height;
        public float distance;
        public float angle;
        [Space]
        public Vector3 offset;
    }
}
