﻿using UnityEngine;

namespace Matchmaking
{
    [CreateAssetMenu(fileName = "MatchSettings", menuName = "Data/Matchmaking/MatchSettings", order = 1)]
    public class MatchSettings : ScriptableObject
    {
        public MatchDifficulty matchDifficulty;
    }
}