﻿namespace Matchmaking
{
    public interface IMatchStateObserver
    {
        public void OnMatchStateChanged(MatchState state);
    }
}