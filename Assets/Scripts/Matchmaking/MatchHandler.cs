﻿using System.Collections.Generic;
using Settings;

namespace Matchmaking
{
    public static class MatchHandler
    {
        static MatchHandler()
        {
            MatchStateObservers = new List<IMatchStateObserver>(5);

            Setup();
        }

        private static void Setup()
        {
            SettingsLoader settingsLoader = new SettingsLoader();
            MatchSettings matchSettings = settingsLoader.Load<MatchSettings>(SettingsKeys.MatchSettingsKey);
            MatchDifficulty = matchSettings.matchDifficulty;
        }

        public static MatchState MatchState { get; private set; }
        public static MatchDifficulty MatchDifficulty { get; private set; }
        
        private static readonly List<IMatchStateObserver> MatchStateObservers;
        
        public static void ChangeStage(MatchState state)
        {
            MatchState = state;
            Notify();
        }

        private static void Notify()
        {
            foreach (var observer in MatchStateObservers)
            {
                observer.OnMatchStateChanged(MatchState);
            }
        }

        public static void Register(IMatchStateObserver matchStateObserver)
        {
            if(MatchStateObservers.Contains(matchStateObserver)) return;

            MatchStateObservers.Add(matchStateObserver);
        }

        public static void Unregister(IMatchStateObserver matchStateObserver)
        {
            if (MatchStateObservers.Contains(matchStateObserver))
            {
                MatchStateObservers.Remove(matchStateObserver);
            }
        }
    }
}