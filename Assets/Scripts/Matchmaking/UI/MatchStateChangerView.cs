﻿using UnityEngine;

namespace Matchmaking.UI
{
    public class MatchStateChangerView : MonoBehaviour, IMatchStateObserver
    {
        private void OnEnable()
        {
            MatchHandler.Register(this);
        }

        private void OnDisable()
        {
            MatchHandler.Unregister(this);
        }

        [SerializeField] private StateChangeCondition[] conditions;
        
        public void OnMatchStateChanged(MatchState state)
        {
            foreach (var condition in conditions)
            {
                condition.CheckCondition(state);
            }
        }
    }
}