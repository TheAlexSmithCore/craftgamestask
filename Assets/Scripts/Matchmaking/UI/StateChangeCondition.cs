﻿using UnityEngine;
using UnityEngine.Events;

namespace Matchmaking.UI
{
    [System.Serializable]
    public struct StateChangeCondition
    {
        [SerializeField] private MatchState matchState;
        [SerializeField] private UnityEvent<bool> conditionEvent;

        public void CheckCondition(MatchState state)
        {
            if (state == matchState)
            {
                conditionEvent.Invoke(true);
            }
            else
            {
                OnConditionExit();
            }
        }

        public void OnConditionExit()
        {
            conditionEvent.Invoke(false);
        }
    }
}