﻿namespace Matchmaking
{
    public enum MatchState
    {
        WaitingForStart = 0,
        Started = 1,
        Ended = 2
    }
}