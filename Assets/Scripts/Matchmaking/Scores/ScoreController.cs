﻿using System;

namespace Matchmaking.Scores
{
    public static class ScoreController
    {
        static ScoreController()
        {
            ScoreModel = new ScoreModel();
        }
        
        public static event Action<int> OnScoreEarned;
        private static readonly ScoreModel ScoreModel;

        public static void ScoreEarned()
        {
            ScoreModel.Add();
            OnScoreEarned?.Invoke(ScoreModel.Get());
        }

        public static void ScoreReset()
        {
            ScoreModel.Reset();
            OnScoreEarned?.Invoke(ScoreModel.Get());
        }
    }
}