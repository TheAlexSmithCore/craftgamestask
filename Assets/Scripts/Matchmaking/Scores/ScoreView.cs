using UnityEngine;
using UnityEngine.UI;

namespace Matchmaking.Scores
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private Text scoreText;

        private void OnEnable()
        {
            ScoreController.OnScoreEarned += ScoreUpdate;
        }

        private void OnDisable()
        {
            ScoreController.OnScoreEarned -= ScoreUpdate;
        }

        private void ScoreUpdate(int scoreCount)
        {
            scoreText.text = scoreCount.ToString();
        }
    }
}
