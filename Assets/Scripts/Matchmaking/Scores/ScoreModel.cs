﻿namespace Matchmaking.Scores
{
    public class ScoreModel
    {
        private int _scoresCount;

        public void Reset()
        {
            _scoresCount = 0;
        }
        
        public void Add()
        {
            _scoresCount++;
        }

        public int Get() => _scoresCount;
    }
}