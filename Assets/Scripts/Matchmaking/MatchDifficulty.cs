﻿namespace Matchmaking
{
    public enum MatchDifficulty
    {
        Easy = 0,
        Normal = 1,
        Hard = 2
    }
}