﻿using System;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Settings
{
    public class SettingsLoader
    {
        private const string SettingsFolderLocation = "Settings";
        
        public async void LoadAsync<TLoadType>(string settingsKey, Action<TLoadType> onResourceLoaded) where TLoadType : ScriptableObject
        {
            ResourceRequest settingsRequest = Resources.LoadAsync<TLoadType>(GetCorrectPathToResource(settingsKey));

            while (!settingsRequest.isDone)
            {
                await Task.Yield();
            }

            if (settingsRequest.asset == null)
            {
                Debug.LogException(new NullReferenceException($"Settings cannot be loaded with the key: {settingsKey}."));                
            }

            TLoadType settingsAsset = (TLoadType)settingsRequest.asset;
            onResourceLoaded.Invoke(settingsAsset);
        }

        public TLoadType Load<TLoadType>(string settingsKey) where TLoadType : ScriptableObject
        {
            return Resources.Load<TLoadType>(GetCorrectPathToResource(settingsKey));
        }

        private string GetCorrectPathToResource(string path)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(SettingsFolderLocation);
            stringBuilder.Append("/");
            stringBuilder.Append(path);

            return stringBuilder.ToString();
        }
    }
}