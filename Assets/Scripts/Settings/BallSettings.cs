﻿using Movement;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "BallParameters", menuName = "Settings/BallParameters")]
    public class BallSettings : ScriptableObject
    {
        public MovementParameters movementParameters;
    }
}