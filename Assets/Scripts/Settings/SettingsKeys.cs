﻿namespace Settings
{
    public static class SettingsKeys
    {
        public const string CameraSettingsKey = "CameraSettings";
        public const string BallSettingsKey = "BallSettings";
        public const string MatchSettingsKey = "MatchSettings";
        public const string MatchDifficultySettingsKey = "MatchDifficultySettings";
        public const string GenerationSettingsKey = "GenerationSettings";
    }
}