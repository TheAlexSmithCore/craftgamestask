﻿using Cameras;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "CameraSettings", menuName = "Settings/CameraSettings")]
    public class CameraSettings : ScriptableObject
    {
        public CameraParameters cameraParameters;
    }
}