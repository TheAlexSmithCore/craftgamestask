﻿using Generators;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "GenerationSettings", menuName = "Settings/GenerationSettings")]
    public class GenerationSettings : ScriptableObject
    {
        public GenerationRule[] rules;
    }
}