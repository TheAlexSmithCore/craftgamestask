﻿namespace Commands
{
    public interface IUndoCommand : ICommand
    {
        public void Undo();
    }
}