﻿using UnityEngine;

namespace Commands
{
    public class ChangeDirectionCommand : ICommand
    {
        public ChangeDirectionCommand(params Vector3[] possibleDirections)
        {
            _possibleDirections = possibleDirections;

            _moveDirection = _possibleDirections[0];
        }

        private int _directionsPointer;
        private Vector3 _moveDirection;

        private readonly Vector3[] _possibleDirections;

        public Vector3 GetResult() => _moveDirection;

        public void Execute()
        {
            if (_directionsPointer == _possibleDirections.Length)
            {
                _directionsPointer = 0;
            }
            
            _moveDirection = _possibleDirections[_directionsPointer];

            _directionsPointer++;
        }
    }
}