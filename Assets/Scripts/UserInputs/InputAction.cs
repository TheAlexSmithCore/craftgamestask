﻿using System;

namespace UserInputs
{
    public class InputAction : IUserInputObserver
    {
        public InputAction(Predicate<bool> inputCondition)
        {
            _inputCondition = inputCondition;
        }

        private event Action ONInputAction;
        private readonly Predicate<bool> _inputCondition;

        public void Register(Action inputAction)
        {
            ONInputAction = inputAction;
        }

        public void Unregister()
        {
            ONInputAction = null;
        }
        
        public void Observe()
        {
            if (_inputCondition(true))
            {
                ONInputAction?.Invoke();
            }
        }
    }
}