﻿using System;
using System.Collections.Generic;
using Matchmaking;
using UnityEngine;

namespace UserInputs
{
    public class UserInput : IMatchStateObserver
    {
        public UserInput()
        {
            InitActions();
        }

        private Dictionary<int, IUserInputObserver> _inputActions = new Dictionary<int, IUserInputObserver>();
        
        private void InitActions()
        {
            int leftMouseButtonHash = "LeftMouseButtonPressed".GetHashCode();
            
            _inputActions = new Dictionary<int, IUserInputObserver>()
            {
                [leftMouseButtonHash] = new InputAction(b => Input.GetMouseButtonDown(0)),
            };
        }

        public void RegisterAction(string actionKey, Action onAction)
        {
            int keyHash = actionKey.GetHashCode();

            if (_inputActions.ContainsKey(keyHash))
            {
                _inputActions[keyHash].Register(onAction);
            }
        }
        
        public void UnregisterAction(string actionKey)
        {
            int keyHash = actionKey.GetHashCode();

            if (_inputActions.ContainsKey(keyHash))
            {
                _inputActions[keyHash].Unregister();
            }
        }

        public void UnregisterAll()
        {
            foreach (var inputObserver in _inputActions.Values)
            {
                inputObserver.Unregister();;
            }
        }
        
        public void Process()
        {
            foreach (var inputAction in _inputActions.Values)
            {
                inputAction.Observe();
            }
        }

        public void OnMatchStateChanged(MatchState state)
        {
        }
    }
}