﻿using System;

namespace UserInputs
{
    public interface IUserInputObserver
    {
        public void Register(Action inputAction);
        public void Unregister();
        public void Observe();
    }
}