﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Environment
{
    public class GemsCollector
    {
        public GemsCollector(Transform playerTarget, float distanceToPickGems, LinkedList<GameObject> gems, Action<GameObject> onGemCollected)
        {
            _playerTarget = playerTarget;
            _gems = gems;

            _distanceToGem = distanceToPickGems;

            ONGemCollected = onGemCollected;
        }
        
        private readonly Transform _playerTarget;
        private readonly LinkedList<GameObject> _gems;
        private readonly float _distanceToGem;

        private event Action<GameObject> ONGemCollected;

        public void Process()
        {
            foreach (var gem in _gems)
            {
                if(!gem.activeSelf) continue;

                float distance = (_playerTarget.position - gem.transform.position).magnitude;

                if (distance < _distanceToGem)
                {
                    ONGemCollected?.Invoke(gem);
                    gem.SetActive(false);
                }
            }
        }
    }
}