﻿using Generators;
using Matchmaking;
using Matchmaking.Scores;
using Pooling;
using Settings;
using UnityEngine;

namespace Environment
{
    public class LevelHandler : MonoBehaviour, IMatchStateObserver
    {
        [SerializeField] private Transform playerTarget;
        [SerializeField] private float distanceToPickGems;
        [Space]
        [SerializeField] private GameObject tilePrefab;
        [SerializeField] private Transform tilesParent;
        [SerializeField] private GameObject gemsPrefab;
        [SerializeField] private Transform gemsParent;
        
        private GemsHandler _gemsHandler;
        private TileGenerator _tileGenerator;
        private GenerateOnDistance _generateOnDistance;

        private void Awake()
        {
            SetupGenerationRule();
            
            MatchHandler.Register(this);
            ScoreController.ScoreReset();
            
            GenerationRule generationRule = SetupGenerationRule();
            
            ObjectPooling tilePooling = new ObjectPooling(tilePrefab, tilesParent, generationRule.tilesPoolCount);
            ObjectPooling gemsPooling = new ObjectPooling(gemsPrefab, gemsParent, generationRule.gemsPoolCount);
            
            GemsGenerator gemsGenerator = new GemsGenerator(gemsPooling, generationRule.gemsGenerationRule, generationRule.gemsPoolCount);
            GemsCollector gemsCollector = new GemsCollector(playerTarget, distanceToPickGems,gemsPooling.GetList(), 
                 (gem)=>
                {
                    ScoreController.ScoreEarned();
                });
            
            _gemsHandler = new GemsHandler(gemsGenerator, gemsCollector);
            _tileGenerator = new TileGenerator(tilePrefab, generationRule, tilePooling, gemsGenerator.TryGenerate);
            
            _generateOnDistance = new GenerateOnDistance(playerTarget, generationRule.minDistanceToGeneration, _tileGenerator.Generate);
        }

        private void Restart()
        {
            _tileGenerator.Restart();
            _gemsHandler.Restart();
        }

        private void OnDestroy()
        {
            MatchHandler.Unregister(this);
        }

        private GenerationRule SetupGenerationRule()
        {
            SettingsLoader settingsLoader = new SettingsLoader();
            GenerationSettings generationSettings = settingsLoader.Load<GenerationSettings>(SettingsKeys.GenerationSettingsKey);

            foreach (var rule in generationSettings.rules)
            {
                if (rule.matchDifficulty == MatchHandler.MatchDifficulty)
                {
                    return rule;
                }
            }

            return default;
        }

        private void Update()
        {
            _generateOnDistance.Update(_tileGenerator.GetPointer());
            _gemsHandler.Update();
        }

        public void OnMatchStateChanged(MatchState state)
        {
            switch (state)
            {
                case MatchState.WaitingForStart:
                    Restart();
                    break;
            }
        }
    }
}