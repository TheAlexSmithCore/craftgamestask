﻿using Entities;
using Generators;

namespace Environment
{
    public class GemsHandler
    {
        public GemsHandler(GemsGenerator gemsGenerator, GemsCollector gemsCollector)
        {
            _gemsGenerator = gemsGenerator;
            _gemsCollector = gemsCollector;
        }
        
        private readonly GemsGenerator _gemsGenerator;
        private readonly GemsCollector _gemsCollector;

        public void Restart()
        {
            _gemsGenerator.Restart();
        }
        
        public void Update()
        {
            _gemsCollector.Process();
        }
    }
}