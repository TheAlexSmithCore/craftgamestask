﻿using Cameras;
using Commands;
using CustomPhysics;
using Matchmaking;
using Movement;
using Settings;
using UnityEngine;
using UserInputs;

namespace Entities
{
    public class Ball : MonoBehaviour, IMatchStateObserver
    {
        [SerializeField] private Camera currentCamera;
        
        private NormalDetector _normalDetector;
        private IMovable _movable;

        private UserInput _userInput;
        private CameraHandler _cameraHandler;
        private EntitySpawner _entitySpawner;

        private SettingsLoader _settingsLoader;

        private void Start()
        {
            Init();
        }

        private void OnDestroy()
        {
            MatchHandler.Unregister(this);
            _userInput.UnregisterAll();
        }

        private void Init()
        {
            MatchHandler.Register(this);
            
            _settingsLoader = new SettingsLoader();

            CameraSettings cameraSettings = _settingsLoader.Load<CameraSettings>(SettingsKeys.CameraSettingsKey);
            CameraParameters cameraParameters = cameraSettings.cameraParameters;
            
            BallSettings ballSettings = _settingsLoader.Load<BallSettings>(SettingsKeys.BallSettingsKey);
            MovementParameters movementParameters = ballSettings.movementParameters;
            
            _cameraHandler = new CameraHandler();
            TopDownCamera topDownCamera = new TopDownCamera(currentCamera.transform, transform, cameraParameters);
            _cameraHandler.SetController(topDownCamera);
            
            ChangeDirectionCommand changeDirectionCommand = new ChangeDirectionCommand(
                Vector3.forward,
                Vector3.right
            );
            
            RayCaster rayCaster = new RayCaster(
                transform, 
                Vector3.down,
                1 << LayerMask.NameToLayer("Default")
            );

            _normalDetector = new NormalDetector(rayCaster);
            BallMovement ballMovement = new BallMovement(transform, _normalDetector, movementParameters);
            _movable = ballMovement;
            
            _entitySpawner = new EntitySpawner(transform.position, _movable);
            
            _userInput = new UserInput();

            _userInput.RegisterAction("LeftMouseButtonPressed",Click);

            void Click()
            {
                if (MatchHandler.MatchState == MatchState.WaitingForStart)
                {
                    MatchHandler.ChangeStage(MatchState.Started);
                }

                if (MatchHandler.MatchState == MatchState.Ended)
                {
                    MatchHandler.ChangeStage(MatchState.WaitingForStart);
                    return;
                }
                
                changeDirectionCommand.Execute();
                Vector3 direction = changeDirectionCommand.GetResult();
                ballMovement.SetDirection(direction);
            }
            
            MatchHandler.ChangeStage(MatchState.WaitingForStart);
        }

        private void DetectGameEnd()
        {
            if (!_normalDetector.IsDetected && MatchHandler.MatchState == MatchState.Started)
            {
                MatchHandler.ChangeStage(MatchState.Ended);
            }
        }

        private void Update()
        {
            _userInput?.Process();
            _movable?.Move();
            _cameraHandler?.Execute();
        }

        private void FixedUpdate()
        {
            if(_normalDetector == null)
                return;

            _normalDetector.Detect();
            
            DetectGameEnd();
        }

        public void OnMatchStateChanged(MatchState state)
        {
            switch (state)
            {
                case MatchState.WaitingForStart:
                    _entitySpawner.Respawn();
                    break;
            }
        }
    }
}