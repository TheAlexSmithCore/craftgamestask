﻿using Matchmaking.Scores;
using UnityEngine;

namespace Entities
{
    public class EntitySpawner
    {
        public EntitySpawner(Vector3 startPoint, IMovable movable)
        {
            _startPoint = startPoint;
            _movable = movable;
        }

        private readonly Vector3 _startPoint;
        private readonly IMovable _movable;

        public void Respawn()
        {
            _movable.TranslateTo(_startPoint);
            ScoreController.ScoreReset();
        }
    }
}