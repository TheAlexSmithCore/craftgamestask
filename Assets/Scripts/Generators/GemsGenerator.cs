﻿using Pooling;
using UnityEngine;

namespace Generators
{
    public class GemsGenerator
    {
        public GemsGenerator(ObjectPooling gemsPooling, int gemsGenerationRule, int gemsPoolCount)
        {
            _gemsGenerationRule = gemsGenerationRule;
            _gemsPooling = gemsPooling;
            _gemsPooling.DisableAll();
        }

        private int _positionPointer;

        private readonly ObjectPooling _gemsPooling;
        private readonly int _gemsGenerationRule;

        public void Restart()
        {
            _gemsPooling.DisableAll();
        }
        
        public void TryGenerate(Vector3 blockPosition)
        {
            if (_gemsGenerationRule == -1)
            {
                GenerateRandom(blockPosition);
            }
            else
            {
                if (_positionPointer % _gemsGenerationRule == 0 && _positionPointer != 0)
                {
                    Generate(blockPosition);
                }
            }

            _positionPointer++;
        }

        private void GenerateRandom(Vector3 blockPosition)
        {
            int randomValue = Random.Range(1, 101);

            if (randomValue % 10 == 0)
            {
                Generate(blockPosition);
            }
        }

        private void Generate(Vector3 blockPosition)
        {
            GameObject gem = _gemsPooling.GetFirstPooledObject();
            gem.transform.position = blockPosition + Vector3.up;
            gem.SetActive(true);
        }
    }
}