﻿using System;
using Pooling;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Generators
{
    public class TileGenerator
    {
        public TileGenerator(GameObject tilePrefab, GenerationRule generationRule, ObjectPooling objectPooling, Action<Vector3> onGenerateGem)
        {
            _tilePrefab = tilePrefab;
            _generationRule = generationRule;
            _objectPooling = objectPooling;
            
            _tileBounds = GetTileBounds();
            ONGenerateGem = onGenerateGem;
        }
        
        private Bounds _tileBounds;
        private Vector3 _pointerPosition;
        private Vector3 _trackDirection;

        private readonly GameObject _tilePrefab;
        private readonly GenerationRule _generationRule;
        private readonly ObjectPooling _objectPooling;

        private event Action<Vector3> ONGenerateGem;

        public void Restart()
        {
            _pointerPosition = Vector3.zero;
            _trackDirection = Vector3.forward;
            
            _objectPooling.DisableAll();
            GeneratePlane(_generationRule.startPlatformWidth, false);
            _pointerPosition = _generationRule.startOffsetDelta * _trackDirection;
            Generate();
        }
        
        private Bounds GetTileBounds()
        {
            MeshRenderer meshRenderer = _tilePrefab.GetComponent<MeshRenderer>();
            return meshRenderer.bounds;
        }
        
        private void MovePointer()
        {
            _pointerPosition += _trackDirection * _generationRule.trackWidth;
        }
        
        private void ChangeGenerationDirection()
        {
            _trackDirection = _trackDirection == Vector3.right ? Vector3.forward : Vector3.right;
        }
        
        public void Generate()
        {
            int generationCount = _generationRule.generationCountPerStep;
            
            for (int i = 0; i < generationCount; i++)
            {
                int randomValue = Random.Range(0, 17);

                if (randomValue % 2 == 0)
                {
                    ChangeGenerationDirection();
                }
                
                GeneratePlane(_generationRule.trackWidth);
                MovePointer();
            }
        }

        private void GeneratePlane(int trackWidth, bool generateWithGem = true)
        {
            // Данная генерация осуществляется через поиск самого крайнего элемента
            float tileSize = _tileBounds.size.x;
            Vector3 center = _pointerPosition;
            
            bool isTrackWidthEven = trackWidth % 2 == 0;
            float halfOfTrackWidth = _generationRule.trackWidth * 0.5f;
            int centerBlock = Mathf.CeilToInt(halfOfTrackWidth);

            Vector3 cornerDirection = GetCornerDirection(center, trackWidth, isTrackWidthEven);

            for (int x = 0; x < trackWidth; x++)
            {
                for (int y = 0; y < trackWidth; y++)
                {
                    GameObject tile = _objectPooling.GetFirstPooledObject();
                    tile.SetActive(true);
                    Vector3 tilePosition = cornerDirection + new Vector3(x, 0, y) * tileSize;
                    tile.transform.position = tilePosition;

                    if (generateWithGem)
                    {
                        if (trackWidth == 1)
                        {
                            GenerateGem(tilePosition);
                            continue;
                        }
                        
                        if (isTrackWidthEven)
                        {
                            GenerateGem(center);
                        }
                        else
                        {
                            if (x == centerBlock)
                            {
                                GenerateGem(tilePosition);
                            }
                        }
                    }
                }
            }
        }

        private Vector3 GetCornerDirection(Vector3 center, int trackWidth, bool isTrackWidthEven)
        {
            float offsetDelta;
            int tilesToCornerCount;

            if(isTrackWidthEven)
            {
                offsetDelta = 0.5f;
                tilesToCornerCount = trackWidth - 1;
            }
            else
            {
                offsetDelta = 1f;
                tilesToCornerCount = trackWidth - 2;
            }

            return center - new Vector3(offsetDelta, 0, offsetDelta) * tilesToCornerCount;
        }

        private void GenerateGem(Vector3 atTilePosition)
        {
            ONGenerateGem?.Invoke(atTilePosition);
        }

        public Vector3 GetPointer() => _pointerPosition;
    }
}