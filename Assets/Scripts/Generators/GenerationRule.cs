﻿using Matchmaking;

namespace Generators
{
    [System.Serializable]
    public struct GenerationRule
    {
        public MatchDifficulty matchDifficulty;
        public int trackWidth;
        public float minDistanceToGeneration;
        public int generationCountPerStep;
        public int tilesPoolCount;
        public int gemsGenerationRule;
        public int gemsPoolCount;

        public int startPlatformWidth;
        public float startOffsetDelta;
    }
}