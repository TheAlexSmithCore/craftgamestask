﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generators
{
    public class TileFallAnimator : MonoBehaviour
    {
        [SerializeField] private int fallPositionLimit;
        
        private Coroutine _coroutine;

        private LinkedList<GameObject> _objectsToFall;

        public void Animate(GameObject tile)
        {
            if (_coroutine == null)
            {
                _coroutine = StartCoroutine(AnimationCoroutine());
            }
            
            _objectsToFall.AddLast(tile);
        }

        IEnumerator AnimationCoroutine()
        {
            while (_objectsToFall.Count > 0)
            {
                foreach (var obj in _objectsToFall)
                {
                    obj.transform.Translate(Vector3.up * -Physics.gravity.y);
                    yield return null;
                    if (obj.transform.position.y < -fallPositionLimit)
                    {
                        _objectsToFall.Remove(obj);
                    }
                }
            }
        }
    }
}