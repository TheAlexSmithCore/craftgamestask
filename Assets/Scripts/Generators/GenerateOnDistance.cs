﻿using System;
using UnityEngine;

namespace Generators
{
    public class GenerateOnDistance
    {
        public GenerateOnDistance(Transform playerTarget, float minDistanceToGeneration, Action onGenerate)
        {
            _playerTarget = playerTarget;
            _minDistanceToGeneration = minDistanceToGeneration;

            ONGenerate = onGenerate;
        }

        private readonly Transform _playerTarget;
        private readonly float _minDistanceToGeneration;
        private event Action ONGenerate;
        
        public void Update(Vector3 pointer)
        {
            var distanceToLastTile = (_playerTarget.position - pointer).magnitude;

            if (distanceToLastTile < _minDistanceToGeneration)
            {
                ONGenerate?.Invoke();
            }
        }
    }
}